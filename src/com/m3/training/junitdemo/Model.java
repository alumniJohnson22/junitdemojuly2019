package com.m3.training.junitdemo;

public class Model implements AutoCloseable {
	
	private IDAO dao;
	private String url;

	public Model(IDAO dao, String url) {
		this.dao = dao;
		this.url = url;
		this.dao.open(this.url);
	}
	
	public Object read() {
		return dao.read("98");
	}
	
	public void calculate() {
		String someObj = new String("This is the business knowledge.");
		dao.write(someObj);
	}

	@Override
	public void close() {
		dao.close();
	}
	

}
