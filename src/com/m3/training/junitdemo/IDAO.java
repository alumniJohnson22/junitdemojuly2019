package com.m3.training.junitdemo;

public interface IDAO {

	void open(String url);

	void close();

	void update(Object object);

	Object read(String id);

	void write(Object object);

	void delete(String id);

}