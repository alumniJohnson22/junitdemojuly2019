package com.m3.training.junitdemo;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ModelTest {

	private IDAO mockDAO;
	private Model objectUnderTest;
	private String url;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		mockDAO = mock(IDAO.class);
		when(mockDAO.read(anyString())).thenReturn("default String");
		when(mockDAO.read("98")).thenReturn("read this fake thing: 98");
		when(mockDAO.read("99")).thenReturn("read this fake thing: 99");

		url = "This.is.fakery.com:1582";
		objectUnderTest = new Model(mockDAO, url);
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void test_Model_createInstance() {
		assertNotNull(objectUnderTest);
	}
	
	@Test
	void test_Model_createInstanceInvokedOpen() {
	    verify(mockDAO).open(url);
	}
	
	@Test
	void test_Model_read() {
		objectUnderTest.read();
	    verify(mockDAO).read("99");
	}
	
	@Test
	void test_Model_readDumpObject() {
		Object object = objectUnderTest.read();
	    System.out.println(object);
	}

}
