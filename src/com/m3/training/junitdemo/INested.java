package com.m3.training.junitdemo;

public interface INested {
	public void doJob1();
	public void doJob2();
}