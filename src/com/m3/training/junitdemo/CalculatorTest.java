package com.m3.training.junitdemo;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

class CalculatorTest {

	private Calculator objectUnderTest;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		// setup here for anything that should be uniform
		// for all the tests
		// System.out.println("before class");
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		// give back any resources that are uniform
		// for all the tests
		// System.out.println("after class");
	}

	@BeforeEach
	void setUp() throws Exception {
		// System.out.println("before each");
		objectUnderTest = new Calculator();
	}

	@AfterEach
	void tearDown() throws Exception {
		// System.out.println("after each");
	}

	@Test
	void test_CalculatorTest_constructor() {
		assertNotNull(objectUnderTest);
	}	
	
	@Test
	void test_CalculatorTest_calculateLetStatement() {
		String input = "Let var = 5.0";
		Double expected = 5.0;
		Double actual = objectUnderTest.calculate(input);
		String msg = "Let statement with assignment of " + expected + " failed";
		assertEquals(expected, actual, msg);
	}
	
	@Test
	void test_CalculatorTest_calculateLetStatementFromVariable() {
		String input = "Let var = 5.0";
		Double expected = 5.0;
		objectUnderTest.calculate(input);
		input = "Let var5 = var";
		String msg = "Let statement with assignment from variable " + input + " failed";
		Double actual = objectUnderTest.calculate(input);
		assertEquals(expected, actual, msg);
	}
	
	@Test
	void test_CalculatorTest_calculateStatementDoesNotStartWithLet() {
		String input = "var = 5.0";
		String msg = "statement with out let in first postion not rejected";
		Executable closure = () -> objectUnderTest.calculate(input);
		assertThrows(IllegalArgumentException.class, closure, msg);
	}
	
	@Test
	void test_CalculatorTest_calculateStatementDoesNotStartWithLetEquals() {
		String input = "let var blablabla 5.0";
		String msg = "statement with no valid = rejected";
		Executable closure = () -> objectUnderTest.calculate(input);
		assertThrows(IllegalArgumentException.class, closure, msg);
	}

	@Test
	void test_CalculatorTest_calculateLetStatementParameterNull() {
		String input = null;
		String msg = "Calculator expected to through an expection for null input failed";
		Executable closure = () -> objectUnderTest.calculate(input);
		assertThrows(IllegalArgumentException.class, closure, msg);
	}
	
	@Test
	void test_CalculatorTest_calculateLetStatementParameterEmpty() {
		String input = "";
		String msg = "Calculator expected to through an expection for empty input failed";
		Executable closure = () -> objectUnderTest.calculate(input);
		assertThrows(IllegalArgumentException.class, closure, msg);
	}

	@Test
	void test_CalculatorTest_calculateLetStatementParameterWhiteSpace() {
		String input = "				";
		String msg = "Calculator expected to through an expection for all whitespace input failed";
		Executable closure = () -> objectUnderTest.calculate(input);
		assertThrows(IllegalArgumentException.class, closure, msg);
	}

	@Test
	void test_CalculatorTest_calculateLetStatementParameterTooBig() {
		StringBuilder sb = new StringBuilder();
		for (int index = 0; index < Calculator.MAX_INPUT + 1; index++) {
			sb.append("Let");
		}
		String input = sb.toString();
		String msg = "Calculator expected to through an expection for all whitespace input failed";
		Executable closure = () -> objectUnderTest.calculate(input);
		assertThrows(IllegalArgumentException.class, closure, msg);
	}
	
	/*
	@Test
	void test_CalculatorTest_fakeSuccess() {
		
	}

	@Test
	void test_CalculatorTest_showException() {
		int var = 6/0;
	}
	*/
}
