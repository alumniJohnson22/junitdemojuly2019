package com.m3.training.junitdemo;

public class DBCnx implements IDAO {
	private String url; 
	/* (non-Javadoc)
	 * @see com.m3.training.junitdemo.IDAO#open(java.lang.String)
	 */
	@Override
	public void open(String url) {
		System.out.println("opening " + url);
		this.url = url;
	}
	
	/* (non-Javadoc)
	 * @see com.m3.training.junitdemo.IDAO#close()
	 */
	@Override
	public void close() {
		System.out.println("opening " + url);		
	}
	
	/* (non-Javadoc)
	 * @see com.m3.training.junitdemo.IDAO#update(java.lang.Object)
	 */
	@Override
	public void update(Object object) {
		System.out.println("update " + object);		
		
	}
	
	/* (non-Javadoc)
	 * @see com.m3.training.junitdemo.IDAO#read(java.lang.String)
	 */
	@Override
	public Object read(String id) {
		return new String(id);
	}
	
	/* (non-Javadoc)
	 * @see com.m3.training.junitdemo.IDAO#write(java.lang.Object)
	 */
	@Override
	public void write(Object object) {
		update(object);
	}
	
	/* (non-Javadoc)
	 * @see com.m3.training.junitdemo.IDAO#delete(java.lang.String)
	 */
	@Override
	public void delete(String id) {
		System.out.println("deleting " + id);
	}
}
