package com.m3.training.junitdemo;

public class Composed {
	
	private INested nestedObject;

	public INested getNestedObject() {
		return nestedObject;
	}

	public void setNestedObject(INested nestedObject) {
		this.nestedObject = nestedObject;
	}

	public Composed(INested nestedObject) {
		super();
		this.nestedObject = nestedObject;
	}
	
	

}
