package com.m3.training.junitdemo;

import java.util.HashMap;
import java.util.Map;

public class Calculator {

	public final static int MAX_INPUT = 100;
	public final static int LET_POSITION = 0;
	public final static int LHS_POSITION = 1;
	public final static int EQUALS_POSITION = 2;
	public final static int RHS_POSITION = 3;
	public final static String LET = "LET";

	private Map<String, Double> globals = new HashMap<>();

	public Double calculate(String input) {
		String msg;
		if (input == null) {
			msg = "Calculator cannot calculate null input.";
			throw new IllegalArgumentException(msg);
		}
		input = input.trim();
		if (input.length() == 0) {
			msg = "Calculator cannot calculate empty input.";
			throw new IllegalArgumentException(msg);
		}
		if (input.length() > Calculator.MAX_INPUT) {
			msg = "Calculator cannot calculate too big input.";
			throw new IllegalArgumentException(msg);
		}
		Double result = 0.0;
		String[] inputAsPieces = input.split("\\s+");
		msg = "Syntax for calculator begins with " + Calculator.LET;
		if (!(inputAsPieces[Calculator.LET_POSITION].toUpperCase().compareTo(Calculator.LET) == 0)) {
			throw new IllegalArgumentException(msg);
		}

		String variableName = inputAsPieces[Calculator.LHS_POSITION];
		if (!(inputAsPieces[Calculator.EQUALS_POSITION].compareTo("=") == 0)) {
			throw new IllegalArgumentException(msg);
		}

		// testing that the RHS is not an expression with operators
		if (inputAsPieces.length == (Calculator.RHS_POSITION + 1)) {
			String key = inputAsPieces[Calculator.RHS_POSITION];
			// assign from one variable to another
			if (globals.containsKey(key)) {
				result = globals.get(key);
			} else {
				// parse a literal
				try {
					result = Double.parseDouble(key);
				} catch (NumberFormatException e) {
					msg = "Cannot assign from alphanumeric value.";
					throw new IllegalArgumentException(msg, e);
				}
			}
		}

		globals.put(variableName, result);
		return result;
	}

	/*
	 * no explicit constructor
	 * 
	 * implicit constructor public Calculator() { super(); }
	 * 
	 */

}
